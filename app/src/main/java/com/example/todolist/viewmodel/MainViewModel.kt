package com.example.todolist.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel: ViewModel() {

    private var _listItemTitles: MutableLiveData<List<String>> = MutableLiveData<List<String>>()
    val listItemTitles: LiveData<List<String>> get() = _listItemTitles

    fun addListItem(list: List<String>) {
        _listItemTitles.value = list
    }
}