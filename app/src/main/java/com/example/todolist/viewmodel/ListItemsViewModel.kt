package com.example.todolist.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.nio.channels.spi.AbstractSelectionKey

class ListItemsViewModel: ViewModel() {

    private var _toDoMap: MutableLiveData<MutableMap<String, List<String>>> = MutableLiveData<MutableMap<String,List<String>>>()
    val toDoMap: LiveData<MutableMap<String, List<String>>> get() = _toDoMap

    private var _listItems: MutableLiveData<List<String>> = MutableLiveData<List<String>>()
    val listItems: LiveData<List<String>> get() = _listItems

    fun addNewMapEntry(listKey: String) {
        val newMap = mutableMapOf<String, List<String>>()
            newMap[listKey] = listItems.value!!
            _toDoMap.value = newMap
    }

    fun addMapEntry(listKey: String) {
        val currentMap = _toDoMap.value!!
        currentMap[listKey] = listItems.value!!
        _toDoMap.value = currentMap
    }
    fun addListItem(list: List<String>) {
        _listItems.value = list
    }
}