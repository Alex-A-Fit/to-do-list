package com.example.todolist.view

import android.os.Bundle
import android.text.Editable
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.todolist.databinding.FragmentToDoListTitlesBinding
import com.example.todolist.util.ToDoListTitleAdapter
import com.example.todolist.viewmodel.MainViewModel


class ToDoListTitlesFragment : Fragment() {
    private var _binding: FragmentToDoListTitlesBinding? = null
    private val binding: FragmentToDoListTitlesBinding get() = _binding!!

    private var listItems: MutableList<String> = mutableListOf()

    private val viewmodel by viewModels<MainViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentToDoListTitlesBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onInitObserver()
        onInitListener()
    }

    private fun onInitObserver() {
    viewmodel.listItemTitles.observe(viewLifecycleOwner){ listItem->
        binding.toDoListTitlesRV.layoutManager = LinearLayoutManager(requireContext())
        binding.toDoListTitlesRV.adapter = ToDoListTitleAdapter(::navigateToListItems).apply {
            getTitles(listItem)
        }
    }
    }

    private fun onInitListener() = with(binding) {
        addItemTitleBtn.setOnClickListener {
            if(ListTitleEditText.text!!.isNotEmpty()){
                val item = ListTitleEditText.text.toString()
                listItems.add(item)
                viewmodel.addListItem(listItems)
                val editable: Editable = SpannableStringBuilder("")
                ListTitleEditText.text = editable
            }

        }
    }
    private fun navigateToListItems(listTitle: String) {
        val action = ToDoListTitlesFragmentDirections.actionToDoListTitlesFragmentToToDoListItemsFragment(listTitle)
        findNavController().navigate(action)
    }
}