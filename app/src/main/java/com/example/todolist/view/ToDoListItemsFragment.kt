package com.example.todolist.view

import android.os.Bundle
import android.text.Editable
import android.text.SpannableStringBuilder
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.todolist.databinding.FragmentToDoListItemsBinding
import com.example.todolist.util.ToDoListItemAdapter
import com.example.todolist.util.ToDoListTitleAdapter
import com.example.todolist.viewmodel.ListItemsViewModel
import com.example.todolist.viewmodel.MainViewModel

class ToDoListItemsFragment : Fragment() {
    private var _binding: FragmentToDoListItemsBinding? = null
    private val binding: FragmentToDoListItemsBinding get() =_binding!!

    private var _listItems: MutableList<String> = mutableListOf()
    private val listItems: List<String> get() = _listItems

    private val viewmodel by viewModels<ListItemsViewModel>()


    private val args by navArgs<ToDoListItemsFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    )= FragmentToDoListItemsBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onInitObserver()
        onInitListener()
    }

    private fun onInitObserver() {
        binding.tvListItems.text = args.listTitle
        viewmodel.listItems.observe(viewLifecycleOwner){ listItem->
            binding.toDoListItemsRV.layoutManager = LinearLayoutManager(requireContext())
            binding.toDoListItemsRV.adapter = ToDoListItemAdapter(::navigateToListItems).apply {
                getListItems(listItem)
            }
        }
    }

    private fun onInitListener() = with(binding) {
        addItemBtn.setOnClickListener {
            if (ListItemsEditText.text!!.isNotEmpty()) {
                val item = ListItemsEditText.text.toString()
                _listItems.add(item)

                Log.d("LIST", "LIST ITEMS: $listItems, ${viewmodel.listItems.value}, ${args.listTitle}")
                viewmodel.addListItem(listItems)
                val editable: Editable = SpannableStringBuilder("")
                ListItemsEditText.text = editable
            }
        }
    }
    private fun navigateToListItems(listTitle: String) {
        val action = ToDoListItemsFragmentDirections.actionToDoListItemsFragmentToToDoListSubItemsFragment()
        findNavController().navigate(action)
    }
}