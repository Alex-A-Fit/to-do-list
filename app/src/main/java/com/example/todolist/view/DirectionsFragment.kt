package com.example.todolist.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.todolist.R
import com.example.todolist.databinding.FragmentDirectionsBinding
import com.example.todolist.databinding.FragmentIntroBinding

class DirectionsFragment : Fragment() {
    private var _binding: FragmentDirectionsBinding? = null
    private val binding: FragmentDirectionsBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDirectionsBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onInitListener()
    }

    private fun onInitListener() = with(binding) {
        btnIntro.setOnClickListener {
        val action = DirectionsFragmentDirections.actionDirectionsFragmentToToDoListTitlesFragment()
        findNavController().navigate(action)
        }
    }
}