package com.example.todolist.util

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.todolist.databinding.ListItemTitlesBinding


class ToDoListTitleAdapter(private val navigate: (title: String) -> Unit
): RecyclerView.Adapter<ToDoListTitleAdapter.viewHolder>() {

    private lateinit var listItemData: List<String>

    class viewHolder(private val binding: ListItemTitlesBinding) : RecyclerView.ViewHolder(binding.root) {
        fun addItem(listItem: String){
            binding.listName.text = listItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val binding = ListItemTitlesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return viewHolder(binding)

    }

    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        val item = listItemData[position]
        holder.itemView.setOnClickListener{
            navigate(item)
        }
        holder.addItem(item)
    }

    override fun getItemCount(): Int {
        return listItemData.size
    }
    fun getTitles(listItem: List<String>){
        listItemData = listItem
    }
}