package com.example.todolist.util

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.todolist.databinding.ListItemsBinding

class ToDoListItemAdapter(private val navigate: (title: String) -> Unit
): RecyclerView.Adapter<ToDoListItemAdapter.viewHolder>() {

    private lateinit var listItemData: List<String>

    class viewHolder(private val binding: ListItemsBinding) : RecyclerView.ViewHolder(binding.root) {
        fun addItem(listItem: String){
            binding.listName.text = listItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val binding = ListItemsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return viewHolder(binding)

    }

    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        val item = listItemData[position]
        holder.itemView.setOnClickListener{
            navigate(item)
        }
        holder.addItem(item)
    }

    override fun getItemCount(): Int {
        return listItemData.size
    }
    fun getListItems(listItem: List<String>){
        listItemData = listItem
        Log.d("ADAPTER", "$listItem")
    }
}